const express = require("express");
const path = require("path");
const app = express();
const port = 8000;

const appRouter = require("./routes/appRouter");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", appRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
