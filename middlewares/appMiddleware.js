//middleware in thoi gian hien tai ra console.log

const printDateMiddleware = (req, res, next) => {
    console.log(new Date());
    next();
}

//in url cua request ra console.log

const printUrlMiddleware = (req, res, next) => {
    console.log(req.url);
    next();
}

module.exports = {
    printDateMiddleware,
    printUrlMiddleware
}