const express = require('express');
const path = require('path');
const router = express.Router();

const {printDateMiddleware,
    printUrlMiddleware } = require('../middlewares/appMiddleware');

router.get('/', printDateMiddleware, printUrlMiddleware,  (req, res) => {
    res.sendFile(path.join(__dirname, '../views/index.html'));
})

module.exports = router;